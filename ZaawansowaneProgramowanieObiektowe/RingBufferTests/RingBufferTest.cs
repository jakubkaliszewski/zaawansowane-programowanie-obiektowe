﻿using System;
using RingBuffer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RingBufferUnitTest
{
    [TestClass]
    public class RingBufferTest
    {
        [TestMethod]
        public void AddElementTest()
        {
            int newElement = 21;
            var ringBuffer = new RingBuffer<int>();

            try
            {
                ringBuffer.Add(newElement);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }

        }

        [TestMethod]
        public void AddPopElementTest()
        {
            int newElement1 = 21;
            int newElement2 = 12;
            var ringBuffer = new RingBuffer<int>();
            var firstExpectedElement = newElement1;
            var secondExpectedElement = newElement2;

            try
            {
                ringBuffer.Add(newElement1);
                ringBuffer.Add(newElement2);

                var firstActualElement = ringBuffer.Pop();
                var secondActualElement = ringBuffer.Pop();

                Assert.AreEqual(firstExpectedElement, firstActualElement);
                Assert.AreEqual(secondExpectedElement, secondActualElement);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void RingBufferOverflow()
        {
            var ringBuffer = new RingBuffer<int>(2);
            int expectedValue = 3;

            try
            {
                ringBuffer.Add(1);
                ringBuffer.Add(2);
                ringBuffer.Add(3);

                int actualValue = ringBuffer.Pop();

                Assert.AreEqual(expectedValue, actualValue);

                actualValue = ringBuffer.Pop();
                expectedValue = 2;

                Assert.AreEqual(expectedValue, actualValue);

            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void CleanRingBuffer()
        {
            var ringBuffer = new RingBuffer<int>(4);
            try
            {
                ringBuffer.Add(1);
                ringBuffer.Add(2);
                ringBuffer.Add(3);
                ringBuffer.Add(4);

                ringBuffer.Clean();
                Assert.IsTrue(ringBuffer.CountOfElements == 0);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void HasAnyElement()
        {
            var ringBuffer = new RingBuffer<int>(4);
            try
            {
                Assert.IsTrue(ringBuffer.CountOfElements == 0);

                ringBuffer.Add(1);
                ringBuffer.Add(2);
                Assert.IsTrue(ringBuffer.CountOfElements == 2);

                ringBuffer.Pop();
                Assert.IsTrue(ringBuffer.CountOfElements == 1);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
    }
}
