﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RingBuffer
{
    interface IRingBuffer<T>
    {
        void Add(T newElement);
        T Pop();
    }
}
