﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RingBuffer
{
    public class RingBuffer<T> : IRingBuffer<T>
    {
        public int Capacity { get; private set; }
        public int CountOfElements { get; private set; }
        private T[] _buffer;
        private int _readElementIndex;
        private int _writeElementIndex;

        public RingBuffer()
        {
            Capacity = 20;
            _buffer = new T[Capacity];
            _readElementIndex = 0;
            _writeElementIndex = 0;

        }

        public RingBuffer(int capacity)
        {
            Capacity = capacity;
            _buffer = new T[Capacity];
        }

        public void Add(T newElement)
        {
            if (_writeElementIndex < Capacity)
            {
                _buffer[_writeElementIndex] = newElement;
                _writeElementIndex++;
                CountOfElements++;
            }
            else
            {
                _writeElementIndex = 0;
                _buffer[_writeElementIndex] = newElement;
            }
        }

        public T Pop()
        {
            void IndexOperations()
            {
                CountOfElements--;
                _readElementIndex++;
            }

            if (_readElementIndex < Capacity)
            {
                T element = _buffer[_readElementIndex];
                IndexOperations();
                return element;
            }
            else
            {
                _readElementIndex = 0;
                T element = _buffer[_readElementIndex];
                IndexOperations();
                return element;
            }


        }

        public void Clean()
        {
            _buffer = new T[Capacity];
            CountOfElements = 0;
            _readElementIndex = 0;
            _writeElementIndex = 0;
        }


    }
}
